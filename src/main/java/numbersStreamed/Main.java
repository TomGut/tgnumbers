package numbersStreamed;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        System.out.println("Instr 3 - Operacje na liczabch rzeczywistych");
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        //inicjalizujemy pusta tablicę dla metody - DOPISAĆ jaką
        double[] numbers = new double[0];
        //po przerobce showStats dodajemy nową zmienną
        Stats stats = null;

        boolean isLoopRunning = true;

        while(isLoopRunning){

            System.out.println("Polecenia\n0 - wyjście z programu\n1 - wygeneruj liczby\n2 - wczytaj plik" +
                    "\n3 - wypisz liczby\n4 - zapisz liczy do pliku\n5 - dodawanie liczby\n6 - mnożenie liczb\n" +
                    "7 - pokaż statystyki\n8 - zapisz statystyki do pliku binarnego\n9 - wczytaj statystyki");

            int command = scanner.nextInt();

            switch (command){
                //wyjście z programu
                case 0: {
                    System.out.println("Wyjście z programu");
                    isLoopRunning = false;
                    break;
                }
                //wygenerowanie liczb wraz z ich zapisem do pliku
                case 1: {

                    generateNumbers(scanner);
                    break;
                }
                //wczytaj plik
                case 2: {

                    numbers = loadNumbers(scanner);
                    break;
                }
                //wypisanie wszystkich liczb z pliku
                case 3: {

                    print(numbers);
                    break;
                }
                //zapisywanie plików
                case 4: {

                    save(scanner, numbers);
                    break;
                }
                //dodawanie
                case 5: {

                    add(scanner, numbers);
                    break;
                }
                //mnożenie
                case 6: {

                    multiply(scanner, numbers);
                    break;
                }
                //pokazanie statystyk
                case 7: {

                    stats = showStat(numbers);
                    print(stats);
                    break;
                }
                //zapis statystyk
                case 8: {
                    //jako że chcemy przechowywać elementy w obiekcie - musimy stworzyc nową klasę
                    saveStats(scanner, stats);
                    break;
                }

                case 9: {
                    stats = loadStats(scanner);
                }
            }
        }
    }
//jako że pplik binarny to korzystamy z serializacji i strumieni (bajtowy i obiektów)
    private static Stats loadStats(Scanner scanner) {
        String file = scanner.next();
        //w tej metodzie analogicznie do save
        try(FileInputStream fis = new FileInputStream(file); ObjectInputStream ois = new ObjectInputStream(fis)){
            return (Stats) ois.readObject();
// znakiem | obsługujemy w ten sam sposób kilka wyjątków
        }catch(IOException | ClassNotFoundException ex){
            System.err.println(ex);
            return null;
        }

    }

    private static void saveStats(Scanner scanner, Stats stats) {
        String file = scanner.next();

        /*strumień wspierający serializację
        fos pozwala zapisac do pliku a oos - pozwala serializować
        object czyta z obiektów któremu przekazujemy fosa
        */
        try(FileOutputStream fos = new FileOutputStream(file); ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(stats);

        }catch(IOException ex){
            System.err.println(ex);
        }

    }

    private static void print(Stats stats) {

        System.out.println("Max: " + stats.getMax());
        System.out.println("Min: " + stats.getMin());
        System.out.println("Średnia: " + stats.getAverage());
    }

    //liczymy najmniejsza, najwieksza, srednia wartosc z tablicy
    //w następnym kroku przerabiamy je na metodę zwracająca wartość zamiast void idzie Stats i wrzucam returna do metody
    private static Stats showStat(double[] numbers) {
        //zeby moc porównywać musimy wziąc jakikolwiek element tablicy - my bierzemy index0
        double min = numbers[0];
        double max = numbers[0];
        double sum = 0;

        for(double n : numbers){

            sum +=n;

            if(n<min){
                min = n;
            }
            if(n > max){
                max = n;
            }
        }
        //liczymy średnią
        double average = sum/ numbers.length;
        //za pomocą konstruktora nowej klasy Stats zwracamy ją jako nowy obiekt z przekazanymi w metodzie showStats parametrami
        return new Stats(min, max, average);
    }

    private static void multiply(Scanner scanner, double[] numbers) {

        System.out.println("Przez jaką wartość przemnożyć wszystkie liczby ?");
        double value = scanner.nextDouble();

        for(int i=0; i<numbers.length; i++){
            numbers[i] *= value;
        }

    }

    private static void add(Scanner scanner, double[] numbers) {

        System.out.println("Jaką wartość dodać do wszystkich liczb ?");
        double value = scanner.nextDouble();

        for(int i=0; i<numbers.length; i++){
            numbers[i] += value;
        }

    }

    private static void save(Scanner scanner, double[] array) {

        System.out.println("Gdzie zapisać plik ?");
        String file = scanner.next();

        try (FileWriter fw = new FileWriter(file)) {
            fw.write(array.length + "\n");

            for (int i = 0; i<array.length; i++) {

                fw.write(array[i] + "\n");
            }

        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    //wypisywanie wszystkich liczb
    private static void print(double[] array) {

        for (double v : array){

            System.out.println(v);

        }
    }

    //metoda z parametrem - nazwaliśmy go tu scanner, zbiezność z naszą zmienną
    private static double[] loadNumbers(Scanner scanner) {

        System.out.println("Podaj ścieżkę do pliku");
        String file = scanner.next();

        // zeby czytac liniowo dorzucamy buffered reader - dekorator nakladany na dowolony reader znakowy w sposob buforowany
        try(FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr)){
            //wczytujemy pierwszy wiersz który w tym wypadku jest parametrem liczb do wygenerowania
            String countText = br.readLine();
            //dostaliśmy stringa i musimy go skonwertowac do inta bo pozniej bedziemy operowac na liczbach a nie stringow
            int count = Integer.parseInt(countText);
            //i uzywamy liczb po konwersji

            double[] numbers = new double[count];

            for(int i=0; i<count; i++){
                String valueText = br.readLine();
                double value = Double.parseDouble(valueText);
                numbers[i] = value;
            }

            return numbers;

        }catch (IOException ex){
            System.err.println(ex);
            //gdyby wyrzuciło ten wyjatek (a metdo musi cos zwrocic) to niech zwroci pusta tablice
            return new double[0];
        }
    }

    //metoda do generowania i zapisu numerów losowych
    private static void generateNumbers(Scanner scanner) {

        System.out.println("Ile liczb rzeczywistych wygenerować ?");
        int count = scanner.nextInt();
        System.out.println("Gdzie je zapisać ?");
        String file = scanner.next();
        Random random = new Random();

        try(FileWriter fw = new FileWriter(file)){
            //tak należy wpisać żeby int/double przekonwertować na stringa, czyli z ""
            fw.write(count + "");
            //znak nowej linii
            fw.write("\n");

            for(int i=0; i<count; i++){

                double value = random.nextDouble() * 100;
                fw.write(value + "");
                fw.write("\n");

            }

        }catch (IOException ex){
            System.err.println(ex);
        }
    }

}

